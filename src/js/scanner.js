/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported getHumanDuration, getPropsAsync, getHtmlWebsitePropsAsync, Server, Wrapper */

const { Gio, GLib, Gst, GstPbutils, Gtk } = imports.gi;
const { Cover } = imports.paintables;
const Gettext = imports.gettext;
// Optional
const GstTag = pkg.checkSymbol('GstTag') ? imports.gi.GstTag : {
    tag_get_language_name: code => code,
    TAG_ID3V2_HEADER_SIZE: 10,
};
const Signals = imports.signals;

let ydlCommand;
const getYdlCommand = function() {
    if (ydlCommand === undefined)
        if (!GLib.spawn_command_line_sync('which youtube-dl')[3])
            ydlCommand = 'youtube-dl';
        else if (!GLib.spawn_command_line_sync('which flatpak-spawn')[3] && !GLib.spawn_command_line_sync('flatpak-spawn --host which youtube-dl')[3])
            // The app is running inside a flatpak sandbox.
            // 'flatpak-spawn --host' needs "--talk-name=org.freedesktop.Flatpak" permission.
            ydlCommand = 'flatpak-spawn --host youtube-dl';
        else
            ydlCommand = null;

    return ydlCommand;
};

// Callback parameters: ok(Boolean), standart_output(String), standart_error(String), exit_status(Number)
const spawnCommandLineAsync = function(commandLine, callback) {
    let success_, argv, pid, stdin, stdout, stderr;

    try {
        [success_, argv] = GLib.shell_parse_argv(commandLine);
        [success_, pid, stdin, stdout, stderr] = GLib.spawn_async_with_pipes(null, argv, null, GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.DO_NOT_REAP_CHILD, null);
    } catch (e) {
        pkg.debug ? logError(e) : log(e.message);
        callback(false);
        return;
    }

    GLib.close(stdin);
    let outUnixStream = new Gio.UnixInputStream({ fd: stdout, close_fd: true });
    let errUnixStream = new Gio.UnixInputStream({ fd: stderr, close_fd: true });
    let outDataStream = new Gio.DataInputStream({ base_stream: outUnixStream, close_base_stream: true });
    let errDataStream = new Gio.DataInputStream({ base_stream: errUnixStream, close_base_stream: true });

    let read = (dataStream, array) => {
        dataStream.read_line_async(GLib.PRIORITY_LOW, null, (dataStream, res) => {
            let [line, length_] = dataStream.read_line_finish_utf8(res);
            if (line !== null) {
                array.push(line);
                read(dataStream, array);
            } else {
                dataStream.close(null);
            }
        });
    };

    let outputs = [];
    let errors = [];

    read(outDataStream, outputs);
    read(errDataStream, errors);

    GLib.child_watch_add(GLib.PRIORITY_DEFAULT, pid, (pid, status) => {
        GLib.spawn_close_pid(pid);
        callback(true, outputs.join('\n'), errors.join('\n'), status);
    });
};

// @duration in mseconds.
var getHumanDuration = function(duration, short = true) {
    let seconds, minutes, hours;

    seconds = parseInt(duration / 1000);
    [minutes, seconds] = [parseInt(seconds / 60), seconds % 60];
    [hours, minutes] = [parseInt(minutes / 60), minutes % 60];

    if (short)
        return (hours ? `${hours >= 10 ? hours : "0" + hours}:` : "") +
            `${minutes >= 10 ? minutes : "0" + minutes}:${seconds >= 10 ? seconds : "0" + seconds}`;

    hours = hours && _("%d %s").format(hours, Gettext.ngettext(_("hour"), _("hours"), hours));
    minutes = (hours || minutes) && _("%d %s").format(minutes, Gettext.ngettext(_("minute"), _("minutes"), minutes));
    seconds = _("%d %s").format(seconds, Gettext.ngettext(_("second"), _("seconds"), seconds));

    return hours ? _("%s %s %s").format(hours, minutes, seconds) :
        minutes ? _("%s %s").format(minutes, seconds) : seconds;
};

const getStringArray = function(tagList, tagName) {
    let array = [];
    for (let i = 0; i < tagList.get_tag_size(tagName); i++) {
        let string = tagList.get_string_index(tagName, i)[1];
        string && string != ' ' && array.push(string);
    }
    return array;
};

const filterProps = function(props) {
    let newProps = {};
    for (let key in props) {
        let value = props[key];
        if (!value || value == 'null' || value == '0' || value == ' ' || value == 'undefined' || typeof value == 'object')
            continue;
        value = value.replace(/&/g, '&amp;').replace(/<p>/g, '').replace(/<\/p>/g, '\n');
        let nick = Gst.tag_get_nick(key);
        newProps[nick.slice(0, 1).toLocaleUpperCase() + nick.slice(1)] = value;
    }
    return newProps;
};

const getWebsiteGeneralProps = function(entry) {
    return filterProps({
        'track': entry.track || '',
        'artist': entry.artist || '',
        'album': entry.album || '',
        'track-number': String(entry.track_number || ''),
        'album-disc-number': String(entry.disk_number || ''),
        'date': String(entry.release_year || '') || String(entry.release_date || ''),
        'chapters': entry.chapters || '',
        'show-name': entry.series || '',
        'show-episode-number': String(entry.season_number || ''),
        'show-season-number': String(entry.episode_number || ''),
        'id': entry.id || '',
        'title': entry.title || '',
        'alternative title': entry.alt_title || '',
        'uploader': entry.uploader || '',
        'uploader id': entry.uploader_id || '',
        'uploader url': entry.uploader_url || '',
        'upload date': entry.upload_date || '',
        'channel': entry.channel || '',
        'channel id': entry.channel_id || '',
        'channel url': entry.channel_url || '',
        'creator': entry.creator || '',
        'license': entry.license || '',
        'websiteDescription': entry.description || '',
        'categories': entry.categories && entry.categories.join(';') || '',
        'keywords': entry.tags && entry.tags.join(';') || '',
        'thumbnail': entry.thumbnail || '',
        'view count': String(entry.view_count || ''),
        'like count': String(entry.like_count || ''),
        'dislike count': String(entry.dislike_count || ''),
        'average rating': String(entry.average_rating || ''),
    });
};

// scan video-sharing websites with youtube-dl
/* eslint-disable camelcase */
const getHtmlWebsiteList = function(ydlOutputs) {
    // ydlOutputs: [uri0, uri1, ..., json]
    let jsonContent = ydlOutputs.pop();
    let websiteMediaUris = ydlOutputs;
    let jsonObject = JSON.parse(jsonContent);
    let list = [];

    // Is there any type other than 'playlist' ?
    if (jsonObject._type && jsonObject._type != 'playlist') {
        log(`website _type is ${jsonObject._type}`);
        return list;
    }

    let entries = jsonObject._type && jsonObject._type == 'playlist' ? jsonObject.entries : [jsonObject];

    entries.forEach((entry, index) => {
        let {
            track, title, artist, creator, uploader,
            album, channel, album_artist, track_number, disk_number,
            playlist, playlist_title, playlist_index,
            genre, duration, thumbnail, webpage_url,
        } = entry;

        list.push({
            title: title || track || "",
            artist: uploader || creator || artist || "",
            album: playlist_title || playlist || channel || album || "",
            albumArtist: album_artist || "",
            trackNumber: String(playlist_index || "") || String(track_number || "") || "",
            albumDiscNumber: String(disk_number || "") || "",
            genre: genre || "",
            duration: String(duration),
            uri: webpage_url,
            info: jsonObject.webpage_url != webpage_url ? jsonObject.webpage_url : "",
            annotation: playlist_title || playlist || "",
            fileName: "",
            isWebsiteHtml: true,
            websiteMediaUri: websiteMediaUris[index],
            websiteThumbnailUri: thumbnail || null,
            websiteGeneralProps: getWebsiteGeneralProps(entry),
        });
    });

    return list;
};
/* eslint-enable camelcase */

// scan feeds with youtube-dl
const getXmlWebsiteList = function(ydlOutputs) {
    // ydlOutputs: [json]
    let jsonObject = JSON.parse(ydlOutputs[0]);
    let list = [];

    // Is there any type other than 'playlist' ?
    if (jsonObject._type && jsonObject._type != 'playlist') {
        log(`website _type is ${jsonObject._type}`);
        return list;
    }

    jsonObject.entries.forEach((entry, index) => {
        list.push({
            title: entry.title || "",
            artist: "",
            album: jsonObject.title || "",
            albumArtist: "",
            trackNumber: String(jsonObject.entries.length - index),
            albumDiscNumber: "",
            genre: "",
            duration: "",
            uri: entry.url,
            info: jsonObject.webpage_url,
            annotation: jsonObject.title + (jsonObject.description && ("\n\n" + jsonObject.description.replace(/&/g, '&amp;').replace(/<p>/g, '').replace(/<\/p>/g, '\n')) || "") || "",
            fileName: "",
            isWebsiteXml: true,
        });
    });

    return list;
};

const getBasicProps = function(tagList, info) {
    return {
        title: tagList.get_string('title')[1] || "",
        artist: getStringArray(tagList, 'artist').join(';'),
        album: tagList.get_string('album')[1] || "",
        albumArtist: getStringArray(tagList, 'album-artist').join(';'),
        trackNumber: String(tagList.get_uint('track-number')[1]) || "",
        albumDiscNumber: String(tagList.get_uint('album-disc-number')[1]) || "",
        genre: getStringArray(tagList, 'genre').join(';'),
        duration: String(parseInt(info.get_duration() / Gst.MSECOND) || "0"),
        uri: info.get_uri(),
    };
};

const getGeneralProps = function(tagList, privates) {
    return filterProps({
        'title': tagList.get_string('title')[1],
        'artist': getStringArray(tagList, 'artist').join(';'),
        'album': tagList.get_string('album')[1],
        'album-artist': getStringArray(tagList, 'album-artist').join(';'),
        'track-number': (tagList.get_uint('track-number')[0] ? tagList.get_uint('track-number')[1] : "") +
                        (tagList.get_uint('track-count')[0] ? " / " + tagList.get_uint('track-count')[1] : ""),
        'album-disc-number': (tagList.get_uint('album-disc-number')[0] ? tagList.get_uint('album-disc-number')[1] : "") +
                             (tagList.get_uint('album-disc-count')[0] ? " / " + tagList.get_uint('album-disc-count')[1] : ""),
        'genre': getStringArray(tagList, 'genre').join(';'),
        'date': !tagList.get_date_time('datetime')[1]?.has_time() && tagList.get_date_time('datetime')[1]?.has_month() ?
                new Date(tagList.get_date_time('datetime')[1].to_iso8601_string()).toLocaleDateString() :
                !tagList.get_date_time('datetime')[1]?.has_time() && tagList.get_date_time('datetime')[1]?.has_year() ?
                String(tagList.get_date_time('datetime')[1].get_year()) : '',
        'datetime': tagList.get_date_time('datetime')[1]?.has_time() ?
                    new Date(tagList.get_date_time('datetime')[1].to_iso8601_string()).toLocaleString() : '',
        'interpreted-by': getStringArray(tagList, 'interpreted-by').join(';'),
        'performer': getStringArray(tagList, 'performer').join(';'),
        'composer': getStringArray(tagList, 'composer').join(';'),
        'conductor': tagList.get_string('conductor')[1],
        'publisher': tagList.get_string('publisher')[1],
        'organization': tagList.get_string('organization')[1],
        'copyright': tagList.get_string('copyright')[1],
        'copyright-uri': tagList.get_string('copyright-uri')[1],
        'license': tagList.get_string('license')[1],
        'license-uri': tagList.get_string('license-uri')[1],
        'comment': String(tagList.get_string('comment')[1]).length >= 150 ? "" : tagList.get_string('comment')[1],
        'extended-comment': tagList.get_string('extended-comment')[1],
        'location': tagList.get_string('location')[1],
        'homepage': tagList.get_string('homepage')[1],
        'contact': tagList.get_string('contact')[1],
        'description': tagList.get_string('description')[1],
        'version': tagList.get_string('version')[1],
        'isrc': tagList.get_string('isrc')[1],
        'serial': String(tagList.get_uint('serial')[1]),
        'encoded-by': tagList.get_string('encoded-by')[1],
        'application-name': tagList.get_string('application-name')[1],
        'keywords': tagList.get_string('keywords')[1],
        'show-name': tagList.get_string('show-name')[1],
        'show-episode-number': String(tagList.get_uint('show-episode-number')[1]),
        'show-season-number': String(tagList.get_uint('show-season-number')[1]),
        'grouping': tagList.get_string('grouping')[1],
        'user-rating': String(tagList.get_uint('user-rating')[1]),
        'private-data': tagList.privateId3v2Data,
    });
};

const getAudioProps = function(tagList, info) {
    let audioInfo = info.get_audio_streams()[0];
    return filterProps({
        'duration': getHumanDuration(info.get_duration() / Gst.MSECOND, false),
        'audio-codec': tagList.get_string('audio-codec')[1],
        'container-format': tagList.get_string('container-format')[1],
        'language-name':
            tagList.get_string('language-code')[1] ? GstTag.tag_get_language_name(tagList.get_string('language-code')[1]) :
            tagList.get_string('language-name')[1],
        [_("Channels")]:
            audioInfo.get_channels() > 2 ? _("%s %d.1").format(_("Surround"), audioInfo.get_channels() - 1) :
            audioInfo.get_channels() == 2 ? _("Stereo") :
            audioInfo.get_channels() == 1 ? _("Mono") :
            "",
        [_("Sample rate")]: audioInfo.get_sample_rate() ? _("%d Hz").format(audioInfo.get_sample_rate()) : "",
        'bitrate': audioInfo.get_bitrate() ? _("%d kbps").format(audioInfo.get_bitrate() / 1000) : "",
        'beats-per-minute': String(tagList.get_double('beats-per-minute')[1]),
        'encoder': tagList.get_string('encoder')[1],
        'encoder-version': String(tagList.get_uint('encoder-version')[1]),
        'midi-base-note': String(tagList.get_uint('midi-base-note')[1]),
    });
};

const getCovers = function(tagList) {
    let covers = [];

    let i = 0;
    let [success, sample] = tagList.get_sample_index('image', i);
    while (success) {
        let buffer = sample.get_buffer();
        let [size, offset, maxSize_] = buffer.get_sizes();
        covers.push(new Cover({ bytes: new GLib.Bytes(buffer.extract_dup(offset, size)) }));
        [success, sample] = tagList.get_sample_index('image', ++i);
    }

    return covers;
};

const getComment = function(tagList) {
    let comment = tagList.get_string('comment')[1];
    return String(comment).length >= 150 ? comment : "";
};

const getLyrics = function(tagList, privates) {
    return tagList.get_string('lyrics')[1] || tagList.privateId3v2XXX;
};

const parsePrivateId3v2Frame = function(tagList) {
    let props = {};

    let i = 0;
    let [success, sample] = tagList.get_sample_index('private-id3v2-frame', i);
    while (success) {
        try {
            let buffer = sample.get_buffer();
            let data = buffer.extract_dup(GstTag.TAG_ID3V2_HEADER_SIZE + 1, buffer.get_size());

            let index = data.indexOf(0);
            let key = data.subarray(0, index).toLiteral();
            let value = data.subarray(index + 1).toLiteral();

            props[key] = value;
        } catch(e) {}

        [success, sample] = tagList.get_sample_index('private-id3v2-frame', ++i);
    }

    if (Object.keys(props).length) {
        if (props['XXX']) {
            tagList.privateId3v2XXX = props['XXX'];
            delete props['XXX'];
        }

        tagList.privateId3v2Data = Object.entries(props).map(([key, value]) => `${key.toLowerCase()}: ${value}`).join(', ');
    }

    return tagList;
};

var getPropsAsync = function(uri) {
    return new Promise((resolve, reject) => {
        let onDiscovered = function(discoverer, info, error) {
            if (error)
                reject(new Error(`${uri}: ${error.message}`));

            let tagList = info.get_tags();
            if (!tagList)
                resolve([[], [], [], null]);

            tagList = parsePrivateId3v2Frame(tagList);

            resolve([getGeneralProps(tagList), getAudioProps(tagList, info), getCovers(tagList), getComment(tagList), getLyrics(tagList)]);
        };

        let discoverer = new GstPbutils.Discoverer();
        discoverer.connect('discovered', onDiscovered);
        discoverer.start();
        discoverer.discover_uri_async(uri);
    });
};

var getHtmlWebsitePropsAsync = function(uri) {
    return new Promise((resolve, reject) => {
        let ydlCommand = getYdlCommand();
        if (!ydlCommand)
            reject(new Error('Could not find youtube-dl command'));

        spawnCommandLineAsync(`${ydlCommand} --get-url --extract-audio --dump-single-json ${uri}`, (success, output, errorMessage, status) => {
            if (success && output && !status)
                resolve(getHtmlWebsiteList(output.split('\n'))[0] || {});
            else
                reject(new Error(errorMessage));
        });
    });
};

// Scanner instance can be used either directly by Wrapper
// or through a DBus service via Server and Client.
// Discovering numerous files has a high cpu cost and freezes the ui.
const Scanner = class {
    static get mimeTypes() {
        if (!this._mimeTypes) {
            let [success_, contents] = Gio.File.new_for_uri(`resource://${pkg.namePath}/mime-types.json`).load_contents(null);
            this._mimeTypes = JSON.parse(contents.toLiteral());
        }

        return this._mimeTypes;
    }

    constructor(application, clientId) {
        // application and clientId are present if scanner is instantiated from a Server instance.
        this.application = application;
        this.clientId = clientId;

        this.queue = [];
        this.list = [];

        GstPbutils.pb_utils_init();
        this.discoverer = new GstPbutils.Discoverer();
        this.discoverer.connect('finished', this._onFinished.bind(this));
        this.discoverer.connect('discovered', this._onDiscovered.bind(this));
    }

    get canQuit() {
        return !this.pendings || !this.pendings.length;
    }

    // Files are splitted by directory and the resulted groups are send to the discoverer one by one.
    // '_onDiscovered' generates properties for each file.
    // 'finished' signal is emitted by the discoverer when all pending 'discovered' callbacks ('_onDiscovered') have been processed.
    // '_onFinished' exports all the props of the pending group and sends the next group to the discoverer.
    // '_onFinished' is initially called to start the loop.
    // The goal is to send tracks to the list view:
    //   - progressively so playback can start before scanning all files (a playlist can contains thousands tracks)
    //   - by group so rows can be sorted (at directory level, then the user can sort the whole playlist if wanted)
    _onFinished() {
        if (this.list.length) {
            this.emit('files-scanned', this.list);
            this.list = [];
            this.pendings = [];
            imports.system.gc();
        }

        if (this.queue.length) {
            this.discoverer.start();
            this.pendings = this.queue.shift();
            this.pendings
                .map(file => file.has_uri_scheme('cdda') ? GLib.filename_to_uri(file.get_path(), null) : file.get_uri())
                .forEach(uri => this.discoverer.discover_uri_async(uri));
        } else {
            this.emit('busy-changed', false);
        }
    }

    _onDiscovered(discoverer, info, error) {
        // Result values for the discovery process: info.get_result()
        if (error && pkg.debug)
            // Do not annoy user with a notification for a file that is probably just non playable.
            log(`${info.get_uri()}: ${error.message}`);

        let tagList = info.get_tags();
        if (!tagList)
            // Not a media file.
            // If mime types are filtered, tagList should not be null.
            return;

        let props = getBasicProps(tagList, info);
        let file = Gio.File.new_for_uri(props.uri);

        if (!props.title)
            try {
                props.fileName = file.query_info('standard::display-name', Gio.FileQueryInfoFlags.NONE, null).get_display_name() || "";
            } catch(e) {
                props.fileName = file.get_basename() || "";
            }

        if (!props.album && file.has_parent(null))
            try {
                props.folderName = file.get_parent().query_info('standard::display-name', Gio.FileQueryInfoFlags.NONE, null).get_display_name() || "";
            } catch(e) {
                props.folderName = file.get_parent().get_basename() || "";
            }

        if ((!props.trackNumber || props.trackNumber == '0') && file.has_uri_scheme('cdda'))
            props.trackNumber = file.get_basename().replace(/\D/g, '');

        this.list.push(props);
    }

    _addFilesToQueue(files) {
        this.queue.push(files);
        if (!this.pendings) {
            this._onFinished();
            this.emit('busy-changed', true);
        }
    }

    _scanHtmlWebsite(file) {
        if (this.application)
            this.application.hold();

        let ydlCommand = getYdlCommand();
        if (!ydlCommand) {
            log('youtube-dl not found');
            this.emit('error-occurred', _("Media extraction from the website failed.\n%s not found.").format("youtube-dl"));
            return;
        }

        this.emit('busy-changed', true);

        spawnCommandLineAsync(`${ydlCommand} --get-url --extract-audio --dump-single-json ${file.get_uri()}`, (success, output, errorMessage, status) => {
            if (success && output && !status) {
                this.emit('files-scanned', getHtmlWebsiteList(output.split('\n')));
            } else if (errorMessage) {
                log(errorMessage);
                this.emit('error-occurred', errorMessage);
            }

            this.emit('busy-changed', false);

            if (this.application)
                this.application.release();
        });
    }

    _scanXmlWebsite(file) {
        if (this.application)
            this.application.hold();

        let ydlCommand = getYdlCommand();
        if (!ydlCommand) {
            log('youtube-dl not found');
            this.emit('error-occurred', _("Media extraction from the website failed.\n%s not found.").format("youtube-dl"));
            return;
        }

        this.emit('busy-changed', true);

        // Since there is no (yet ?) specific extractor for feeds, use straight the generic extractor.
        // '--flat-playlist': do not scan each entry, knowing that there will be no further information.
        spawnCommandLineAsync(`${ydlCommand} --force-generic-extractor --flat-playlist --dump-single-json ${file.get_uri()}`, (success, output, errorMessage, status) => {
            if (success && output && !status) {
                this.emit('files-scanned', getXmlWebsiteList(output.split('\n')));
            } else if (errorMessage) {
                log(errorMessage);
                this.emit('error-occurred', errorMessage);
            }

            this.emit('busy-changed', false);

            if (this.application)
                this.application.release();
        });
    }

    _scanPlaylist(file) {
        // TotemPlParser does not work with GJS.
    }

    _scanJspf(file, recursive) {
        if (this.application)
            this.application.hold();

        let success_, contents, files;
        try {
            [success_, contents] = file.load_contents(null);
            if (contents instanceof Uint8Array)
                contents = contents.toLiteral();

            files = JSON.parse(contents).playlist.track
                    .map(track => Gio.File.new_for_uri(track.location));
        } catch(e) {
            if (pkg.debug)
                logError(e, `Cannot parse playlist ${file.get_parse_name()}`);
            else
                log(`Cannot parse playlist ${file.get_parse_name()}: ${e.message}`);
        }

        if (files && files.length)
            this.scanFiles(files, recursive);

        if (this.application)
            this.application.release();
    }

    _scanDir(dir, recursive) {
        if (this.application)
            this.application.hold();

        let files = [];
        let subdirs = [];
        let enumerator;
        try {
            enumerator = dir.enumerate_children('standard::type,standard::content-type', Gio.FileQueryInfoFlags.NONE, null);
        } catch(e) {}

        if (enumerator) {
            let fileInfo = enumerator.next_file(null);
            while (fileInfo) {
                if (fileInfo.get_file_type() == Gio.FileType.DIRECTORY)
                    subdirs.push(enumerator.get_child(fileInfo));
                else if (fileInfo.get_file_type() == Gio.FileType.REGULAR &&
                    (this.constructor.mimeTypes.audio.includes(fileInfo.get_content_type())) ||
                    (this.constructor.mimeTypes.video.includes(fileInfo.get_content_type())))
                    files.push(enumerator.get_child(fileInfo));
                fileInfo = enumerator.next_file(null);
            }
            enumerator.close(null);
        }

        if (files.length)
            this._addFilesToQueue(files);
        if (recursive)
            subdirs.forEach(dir => this._scanDir(dir, recursive));

        if (this.application && !files.length)
            this.application.release();
    }

    scanFiles(files, recursive) {
        if (this.application)
            this.application.hold();

        // Verify existence and extract directories.
        let dirs = [];
        let jspfs = [];
        let playlists = [];
        let xmlWebsites = [];
        let htmlWebsites = [];
        files = files.filter(file => {
            // We want an error message (to display to the user) if the file does not exist.
            // query_exists and query_file_type don't provide an error.
            try {
                let fileInfo = file.query_info('standard::type,standard::content-type', Gio.FileQueryInfoFlags.NONE, null);
                Gtk.RecentManager.get_default().add_item(file.get_uri());
                if (fileInfo.get_file_type() == Gio.FileType.DIRECTORY)
                    dirs.push(file);
                // There is no mime type for jspf files.
                else if (fileInfo.get_file_type() == Gio.FileType.REGULAR &&
                    file.get_basename().toLowerCase().endsWith('.jspf'))
                    jspfs.push(file);
                else if (fileInfo.get_file_type() == Gio.FileType.REGULAR &&
                    this.constructor.mimeTypes.playlist.includes(fileInfo.get_content_type()))
                    playlists.push(file);
                else if (this.constructor.mimeTypes.feed.includes(fileInfo.get_content_type()) &&
                    (file.has_uri_scheme('https') || file.has_uri_scheme('http')))
                    // Podcast feed
                    xmlWebsites.push(file);
                else if (fileInfo.get_content_type() == 'text/html')
                    htmlWebsites.push(file);
                else if (fileInfo.get_file_type() == Gio.FileType.REGULAR &&
                    (this.constructor.mimeTypes.audio.includes(fileInfo.get_content_type())) ||
                    (this.constructor.mimeTypes.video.includes(fileInfo.get_content_type())))
                    return true;
                return false;
            } catch(e) {
                if (e.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_SUPPORTED)) {
                    if (pkg.debug)
                        logError(e, `"--talk-name=org.gtk.vfs.*" permission is missing`);
                    else
                        log(`"--talk-name=org.gtk.vfs.*" permission is missing: ${e.message}`);
                    return true;
                } else {
                    if (pkg.debug)
                        logError(e, `Cannot scan ${file.get_parse_name()}`);
                    else
                        log(`Cannot scan ${file.get_parse_name()}: ${e.message}`);
                    this.emit('error-occurred', e.message);
                    return false;
                }
            }
        });

        if (files.length)
            this._addFilesToQueue(files);
        dirs.forEach(dir => this._scanDir(dir, recursive));
        jspfs.forEach(file => this._scanJspf(file, recursive));
        playlists.forEach(file => this._scanPlaylist(file));
        xmlWebsites.forEach(file => this._scanXmlWebsite(file));
        htmlWebsites.forEach(file => this._scanHtmlWebsite(file));

        if (this.application && !files.length)
            this.application.release();
    }
};
Signals.addSignalMethods(Scanner.prototype);

var Server = class {
    static objectPath = `${pkg.namePath}/scanner`; //`
    static wellkownName = `${pkg.name}.scanner`;

    static get ifaceXml() {
        if (!this._ifaceXml) {
            let [success_, contents] = Gio.File.new_for_uri(`resource://${pkg.namePath}/dbus-interfaces/${pkg.name}.scanner.xml`).load_contents(null);
            this._ifaceXml = contents.toLiteral();
        }

        return this._ifaceXml;
    }

    constructor(application) {
        Gst.init(null);
        this.application = application;
        // Gio.DBusExportedObject: Gjs override.
        this.implementation = Gio.DBusExportedObject.wrapJSObject(this.constructor.ifaceXml, this);
        this.scanners = [];

        GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 60, () => {
            if (this.scanners.every(scanner => scanner.canQuit))
                this.application.quit();
            return GLib.SOURCE_CONTINUE;
        });
    }

    export(connection, path) {
        // 'path' should equal Server.objectPath.
        return this.implementation.export(connection, path);
    }

    unexport(connection) {
        return this.implementation.unexport_from_connection(connection);
    }

    _onBusyChanged(scanner, busy) {
        this.implementation.emit_signal('BusyChanged', GLib.Variant.new('(ub)', [scanner.clientId, busy]));

        if (!busy && this.scanners.every(scanner => scanner.canQuit))
            this.application.quit();
    }

    _onFilesScanned(scanner, list) {
        // 'a{ss}': dictionnary with 's' (string) keys and 's' values.
        // 'aa{ss}': array of dictionnaries.
        let tuppleVariant = GLib.Variant.new('(uaa{ss})', [scanner.clientId, list]);
        this.implementation.emit_signal('FilesScanned', tuppleVariant);
    }

    _onErrorOccurred(scanner, message) {
        this.implementation.emit_signal('ErrorOccurred', GLib.Variant.new('(us)', [scanner.clientId, message]));
    }

    // 'clientId' identifies the dbus client: one scanner per window.
    ScanFiles(uris, recursive, clienId = 0) {
        let scanner = this.scanners.reduce((def, scanner) => scanner.clientId == clienId ? scanner : def, null);
        if (!scanner) {
            scanner = new Scanner(this.application, clienId);
            this.scanners.push(scanner);
            scanner.connect('busy-changed', this._onBusyChanged.bind(this));
            scanner.connect('files-scanned', this._onFilesScanned.bind(this));
            scanner.connect('error-occurred', this._onErrorOccurred.bind(this));
        }

        scanner.scanFiles(uris.map(uri => Gio.File.new_for_uri(uri)), recursive);
    }
};

const Client = class {
    static scannerServiceErrorMessage = `No ${Server.wellkownName} service`; //`

    static getProxy(connection) {
        if (!this._ProxyWrapper)
            this._ProxyWrapper = Gio.DBusProxy.makeProxyWrapper(Server.ifaceXml);

        let proxy = new this._ProxyWrapper(connection, Server.wellkownName, Server.objectPath);
        if (!proxy.get_name_owner())
            throw(new Error(this.scannerServiceErrorMessage));

        return proxy;
    }

    constructor(windowId) {
        let connection = Gio.DBus.session;
        // id = process id (multi-instance support) + window id (multi-window support)
        this.clientId = connection.get_unique_name().split('.').pop() + windowId;
        this.proxy = this.constructor.getProxy(connection);
        this.proxy.connectSignal('BusyChanged', this._relaySignal.bind(this, 'busy-changed'));
        this.proxy.connectSignal('FilesScanned', this._relaySignal.bind(this, 'files-scanned'));
        this.proxy.connectSignal('ErrorOccurred', this._relaySignal.bind(this, 'error-occurred'));
    }

    _relaySignal(name, proxy_, proc_, tupple) {
        if (tupple[0] == this.clientId)
            this.emit(name, tupple[1]);
    }

    scanFiles(files, recursive) {
        // 'Sync' is a DBus keyword, properties are got asynchronisly.
        this.proxy.ScanFilesSync(files.map(file => file.get_uri()), recursive, this.clientId);
    }
};
Signals.addSignalMethods(Client.prototype);

var Wrapper = class Wrapper {
    constructor(windowId) {
        this.windowId = windowId;
    }

    _relaySignal(name, emitter_, ...args) {
        this.emit(name, ...args);
    }

    // @index and @play for MPRIS.
    scanFiles(files, recursive, index, play) {
        if (recursive && !this.hasNotDBusService) {
            if (!this.client) {
                try {
                    this.client = new Client(this.windowId);
                } catch(e) {
                    // No DBus service with GNOME Builder Flatpak
                    if (e.message == Client.scannerServiceErrorMessage || !pkg.debug)
                        log(e.message);
                    else
                        logError(e);
                    this.hasNotDBusService = true;
                    this.scanFiles(files, recursive);
                    return;
                }
                this.client.connect('busy-changed', this._relaySignal.bind(this, 'busy-changed'));
                this.client.connect('error-occurred', this._relaySignal.bind(this, 'error-occurred'));
                this.client.connect('files-scanned', (emitter_, list) => this.emit('files-scanned', list, index, play));
            }
            this.client.scanFiles(files, recursive);
        } else {
            let scanner = new Scanner();
            scanner.connect('busy-changed', this._relaySignal.bind(this, 'busy-changed'));
            scanner.connect('error-occurred', this._relaySignal.bind(this, 'error-occurred'));
            scanner.connect('files-scanned', (emitter_, list) => this.emit('files-scanned', list, index, play));
            scanner.scanFiles(files, recursive);
        }
    }
};
Signals.addSignalMethods(Wrapper.prototype);
