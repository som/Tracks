/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Track, Playlist, Playlists */

const { Gio, GLib, GObject } = imports.gi;
const { getHumanDuration, getHtmlWebsitePropsAsync, getPropsAsync } = imports.scanner;

const DIRECTORY = Gio.File.new_for_path(GLib.build_filenamev([GLib.get_user_data_dir(), pkg.shortName.toLowerCase()]));
const QUERY_ATTRIBUTES = 'standard::name,standard::edit-name,standard::display-name,standard::content-type,time::modified';
const PERSISTENT_PLAYLIST_NAME = 'persistent-playlist';

const stripExtension = function(name) {
    return name.slice(0, name.lastIndexOf('.'));
};

const getMatchStrings = function(string) {
    return GLib.str_tokenize_and_fold(string, null).map(token => token.toString()).filter(tokenString => tokenString != '');
};

var Track = GObject.registerClass({
    GTypeName: `${pkg.shortName}Track`,
    Properties: {
        'album': GObject.ParamSpec.string(
            'album', "Album", "The album the track belongs to",
            GObject.ParamFlags.READWRITE, ''
        ),
        'album-artist': GObject.ParamSpec.string(
            'album-artist', "Album artist", "The artist(s) of the entire album",
            GObject.ParamFlags.READWRITE, ''
        ),
        'album-disc-number': GObject.ParamSpec.uint(
            'album-disc-number', "Album disc number", "The number of the disc the track belongs to",
            GObject.ParamFlags.READWRITE, 0, GLib.MAXUINT32, 0
        ),
        'annotation': GObject.ParamSpec.string(
            'annotation', "Annotation", "An annotation field",
            GObject.ParamFlags.READWRITE, ''
        ),
        'artist': GObject.ParamSpec.string(
            'artist', "Artist", "The person(s) responsible for the recording of the track",
            GObject.ParamFlags.READWRITE, ''
        ),
        'duration': GObject.ParamSpec.uint(
            'duration', "Duration", "The length of the track in mseconds",
            GObject.ParamFlags.READWRITE, 0, GLib.MAXUINT32, 0
        ),
        'file-name': GObject.ParamSpec.string(
            'file-name', "File name", "The name of the track location file (fallback)",
            GObject.ParamFlags.READWRITE, ''
        ),
        'folder-name': GObject.ParamSpec.string(
            'folder-name', "Folder name", "The name of the track location folder (fallback)",
            GObject.ParamFlags.READWRITE, ''
        ),
        'genre': GObject.ParamSpec.string(
            'genre', "Genre", "The musical genre(s) the track belongs to",
            GObject.ParamFlags.READWRITE, ''
        ),
        'info': GObject.ParamSpec.string(
            'info', "Info", "An info field",
            GObject.ParamFlags.READWRITE, ''
        ),
        'is-website-html': GObject.ParamSpec.boolean(
            'is-website-html', "Is website html", "Is website html",
            GObject.ParamFlags.READWRITE, false
        ),
        'is-website-xml': GObject.ParamSpec.boolean(
            'is-website-xml', "Is website xml", "Is website xml",
            GObject.ParamFlags.READWRITE, false
        ),
        'title': GObject.ParamSpec.string(
            'title', "Title", "The title of the track",
            GObject.ParamFlags.READWRITE, ''
        ),
        'track-number': GObject.ParamSpec.uint(
            'track-number', "Track number", "The number of track",
            GObject.ParamFlags.READWRITE, 0, GLib.MAXUINT32, 0
        ),
        'uri': GObject.ParamSpec.string(
            'uri', "uri", "The location of the track as a URI",
            GObject.ParamFlags.READWRITE, ''
        ),
    },
}, class extends GObject.Object {
    static get gPropertyKeys() {
        return Object.keys(this[GObject.properties]);
    }

    _init(params) {
        // These props are expirable and consequently not passed to JSON parsing.
        let { websiteMediaUri, websiteThumbnailUri, websiteGeneralProps } = params;
        delete params.websiteMediaUri;
        delete params.websiteThumbnailUri;
        delete params.websiteGeneralProps;

        Object.assign(super._init(params), { websiteMediaUri, websiteThumbnailUri, websiteGeneralProps });
    }

    get _terms() {
        if (!this.__terms)
            this.__terms = [].concat(
                ...['album', 'album-artist', 'artist', 'file-name', 'folder-name', 'genre', 'title'].map(key => getMatchStrings(this[key]))
            );

        return this.__terms;
    }

    compare(track) {
        let [a, b] = [this, track];

        if ((a.album || a.folderName) != (b.album || b.folderName)) {
            return (a.albumArtist || a.artist || a.album || a.folderName).localeCompare(b.albumArtist || b.artist || b.album || b.folderName)
                || (a.album || a.folderName).localeCompare(b.album || b.folderName);
        }

        return Math.sign(a.albumDiscNumber - b.albumDiscNumber)
            || Math.sign(a.trackNumber - b.trackNumber)
            || a.artist.localeCompare(b.artist)
            || a.title.localeCompare(b.title)
            || a.fileName.localeCompare(b.fileName);
    }

    match(searchTerms) {
        return searchTerms.every(group => {
            return group.some(searchTerm => {
                return this._terms.some(term => {
                    return term.includes(searchTerm);
                });
            });
        });
    }

    toJSON() {
        return Object.fromEntries(
            this.constructor.gPropertyKeys.map(key => [key, this[key] || undefined])
        );
    }

    toJSPF() {
        return {
            'location': this.uri,
            'title': this.title,
            'creator': this.artist || this.albumArtist || _("Unkown"),
            'album': this.album,
            'trackNum': this.trackNumber,
            'duration': this.duration,
            'info': this.info || undefined,
            'annotation': this.annotation || undefined,
        };
    }

    get humanDuration() {
        return Number(this.duration) <= 100000000 ? getHumanDuration(this.duration) : "";
    }

    async getPropsAsync() {
        return await getPropsAsync(this.isWebsiteHtml ? this.websiteMediaUri : this.uri);
    }

    async getWebsitePropsAsync() {
        if (!this.isWebsiteHtml)
            return [this.uri, null, null];

        if (!this.websiteMediaUri)
            [this.websiteMediaUri, this.websiteThumbnailUri, this.websiteGeneralProps] = await getHtmlWebsitePropsAsync(this.uri);

        return [this.websiteMediaUri, this.websiteThumbnailUri, this.websiteGeneralProps];
    }
});

var Playlist = GObject.registerClass({
    GTypeName: `${pkg.shortName}Playlist`,
    Implements: [Gio.ListModel],
    Properties: {
        'creator': GObject.ParamSpec.string(
            'creator', "Creator", "The creator of the playlist",
            GObject.ParamFlags.READWRITE, ''
        ),
        'date': GObject.ParamSpec.string(
            'date', "Date", "The date the playlist has been created, in iso8601 format",
            GObject.ParamFlags.READWRITE, ''
        ),
        'display-name': GObject.ParamSpec.string(
            'display-name', "Display name", "The dispay name of the playlist",
            GObject.ParamFlags.READWRITE, ''
        ),
        'edit-name': GObject.ParamSpec.string(
            'edit-name', "Edit name", "The edit name of the playlist",
            GObject.ParamFlags.READWRITE, ''
        ),
        'file': GObject.ParamSpec.object(
            'file', "File", "The file containing the playlist data",
            GObject.ParamFlags.READWRITE, Gio.File.$gtype
        ),
        'name': GObject.ParamSpec.string(
            'name', "Name", "The name of the playlist",
            GObject.ParamFlags.READWRITE, ''
        ),
    },
}, class extends GObject.Object {
    _loadJSON() {
        try {
            let [success_, contents] = this.file.load_contents(null);
            let jsonObject = JSON.parse(contents.toLiteral());
            this.creator = jsonObject.playlist.creator;
            this.date = jsonObject.playlist.date;
            this._tracks = jsonObject.playlist.track.map(props => new Track(props));
        } catch(e) {
            pkg.debug ? logError(e) : log(e.message);
        }
    }

    get _items() {
        if (this._tracks === undefined) {
            if (this.file)
                this._loadJSON();
            else
                this._tracks = [];
        }

        return this._tracks || [];
    }

    vfunc_get_item_type() {
        return Track.$gtype;
    }

    vfunc_get_item(position) {
        return this._items[position] || null;
    }

    vfunc_get_n_items() {
        return this._items.length;
    }

    toJSON() {
        return {
            'playlist': {
                'title': this.name,
                'creator': this.creator,
                'date': this.date,
                'track': this._items,
            }
        };
    }

    toJSPF() {
        return {
            'playlist': {
                'title': this.name,
                'creator': this.creator,
                'date': this.date,
                'track': this._items.map(track => track.toJSPF()),
            },
        };
    }

    [Symbol.iterator]() {
        return this._items[Symbol.iterator]();
    }

    get creator() {
        if (this._creator === undefined)
            this._loadJSON();

        return this._creator;
    }

    set creator(creator) {
        if (this._creator !== creator) {
            this._creator = creator;
            this.notify('creator');
        }
    }

    get date() {
        if (this._date === undefined)
            this._loadJSON();

        return this._date;
    }

    set date(date) {
        if (this._date !== date) {
            this._date = date;
            this.notify('date');
        }
    }

    clear() {
        let length = this._items.length;
        if (length) {
            this._tracks = [];
            this.items_changed(0, length, 0);
        }
    }

    copy() {
        return this.name == PERSISTENT_PLAYLIST_NAME ? this :
            Object.assign(new Playlist({ name: this.name }), { _tracks: this._tracks?.slice(0) || [] });
    }

    deleteAsync() {
        return new Promise((resolve, reject) => {
            try {
                resolve(this.file.delete(null));
            } catch(e) {
                pkg.debug ? logError(e, "Cannot remove the playlist") : log(`Cannot remove the playlist: ${e.message}`);
                reject(new Error(`Cannot remove the playlist: ${e.message}`));
            }
        });
    }

    equal(playlist) {
        return JSON.stringify(this._items) == JSON.stringify([...playlist]);
    }

    /*
     * jspf specification:
     * http://xspf.org/jspf
     * http://xspf.org/xspf-v1.html
     */
    exportAsync(file) {
        return new Promise((resolve, reject) => {
            let contents = JSON.stringify(this.toJSPF(), null, 2);
            try {
                // Get invalid characters with replace_contents_async.
                resolve(file.replace_contents(contents, null, false, Gio.FileCreateFlags.NONE, null)[0]);
            } catch(e) {
                // Probably Gio.IOErrorEnum.PERMISSION_DENIED
                (pkg.debug) ? logError(e, "Cannot export the playlist") : log(`Cannot export the playlist: ${e.message}`);
                reject(new Error(`Cannot export the playlist: ${e.message}`));
            }
        });
    }

    insert(list, index) {
        let tracks = list
            .map(props => new Track(props))
            .sort((track1, track2) => {
                return track1.isWebsiteXml ? track2.compare(track1) : track1.compare(track2);
            });

        this._items.splice(index, 0, ...tracks);
        this.items_changed(index, 0, list.length);
    }

    remove(track) {
        let index = this._items.indexOf(track);
        if (index != -1) {
            this._items.splice(index, 1);
            this.items_changed(index, 1, 0);
        }
    }

    saveAsync(name) {
        return new Promise((resolve, reject) => {
            if (!this._tracks?.length && !DIRECTORY.get_child(`${name}.json`).query_exists(null))
                resolve(false);

            this.creator = GLib.get_user_name();
            this.date = new Date().toISOString();
            this.name = name;
            let contents = JSON.stringify(this, null, 2);

            try {
                if (!DIRECTORY.query_exists(null))
                    DIRECTORY.make_directory_with_parents(null);

                let file = DIRECTORY.get_child(`${name}.json`);
                resolve(file.replace_contents(contents, null, false, Gio.FileCreateFlags.NONE, null)[0]);
            } catch(e) {
                (pkg.debug) ? logError(e, "Cannot save the playlist") : log(`Cannot save the playlist: ${e.message}`);
                reject(new Error(`Cannot save the playlist: ${e.message}`));
            }
        });
    }

    async saveAsDefaultAsync() {
        return await this.saveAsync(PERSISTENT_PLAYLIST_NAME);
    }
});

var Playlists = GObject.registerClass({
    GTypeName: `${pkg.shortName}Playlists`,
    Implements: [Gio.ListModel],
    Properties: {
        // Internal usage.
        // At startup, wait the enumeration is finished to get the default playlist.
        'loading': GObject.ParamSpec.boolean(
            'loading', "Loading", "Whether the playlist enumeration is currently in progress",
            GObject.ParamFlags.READWRITE, false
        ),
    },
}, class extends GObject.Object {
    _init() {
        super._init();

        this._items = [];

        this._enumerateDirectory();
        this._monitorDirectory();
    }

    _clear() {
        let length = this._items.length;
        if (length) {
            this._items = [];
            this.items_changed(0, length, 0);
        }
    }

    _remove(file) {
        let index = this._items.findIndex(playlist => playlist.name + '.json' == file.get_basename());
        if (index >= 0) {
            this._items.splice(index, 1);
            this.items_changed(index, 1, 0);
        }
    }

    _add(file, fileInfo) {
        if (fileInfo.get_content_type() != 'application/json')
            return;

        let name = stripExtension(fileInfo.get_name());
        let playlist = new Playlist({
            displayName: stripExtension(fileInfo.get_display_name()),
            editName: stripExtension(fileInfo.get_edit_name()),
            file, name,
        });

        if (name == PERSISTENT_PLAYLIST_NAME) {
            this._defaultPlaylist = playlist;
            return;
        }

        let index = this._items.findIndex(playlist => playlist.name == name);
        if (index == -1)
            index = this._items.length;

        this._items[index] = playlist;
        this.items_changed(index, 1, 1);
    }

    _enumerateDirectory() {
        this.loading = true;
        DIRECTORY.enumerate_children_async(QUERY_ATTRIBUTES, Gio.FileQueryInfoFlags.NONE, GLib.PRIORIRTY_DEFAULT, null, (source, result) => {
            try {
                let fileInfo, enumerator = source.enumerate_children_finish(result);
                while ((fileInfo = enumerator.next_file(null)))
                    this._add(enumerator.get_child(fileInfo), fileInfo);
                enumerator.close(null);
            } catch(e) {
                // The directory does not exist.
                log(e);
            } finally {
                this.loading = false;
            }
        });
    }

    _onDirectoryChanged(monitor_, file, otherFile_, eventType) {
        switch (eventType) {
            case Gio.FileMonitorEvent.ATTRIBUTE_CHANGED:
            case Gio.FileMonitorEvent.CREATED:
                if (file.equal(DIRECTORY))
                    this._enumerateDirectory();
                else
                    try {
                        this._add(file, file.query_info(QUERY_ATTRIBUTES, Gio.FileQueryInfoFlags.NONE, null));
                    } catch(e) {
                        // file was a temp file and has been removed.
                    }

                break;
            case Gio.FileMonitorEvent.DELETED:
                if (file.equal(DIRECTORY))
                    this._clear();
                else
                    this._remove(file);
        }
    }

    _monitorDirectory() {
        if ((this._monitor = DIRECTORY.monitor_directory(Gio.FileMonitorFlags.NONE, null)))
            this._monitor.connect('changed', this._onDirectoryChanged.bind(this));
    }

    vfunc_get_item_type() {
        return Playlist.$gtype;
    }

    vfunc_get_item(position) {
        return this._items[position] || null;
    }

    vfunc_get_n_items() {
        return this._items.length;
    }

    [Symbol.iterator]() {
        return this._items[Symbol.iterator]();
    }

    getDefaultPlaylistAsync() {
        return new Promise((resolve, reject) => {
            if (this.loading) {
                let handler = this.connect('notify::loading', () => {
                    this.disconnect(handler);
                    resolve(this._defaultPlaylist);
                });
            } else {
                resolve(this._defaultPlaylist);
            }
        });
    }

    getEmptyPlaylist() {
        return new Playlist({ name: PERSISTENT_PLAYLIST_NAME });
    }

    // For dialogs and MPRIS.
    getPlaylist(name) {
        return this._items.find(playlist => playlist.name == name);
    }

    disable() {
        if (this._monitor)
            this._monitor.cancel();
    }
});
