/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Cover, Spectrum */

const { Gdk, GdkPixbuf, Gio, GLib, GObject, Graphene, Gtk, Pango } = imports.gi;

// Wrapper around cover bytes (from tags) or file (from web media thumbnail) that generates
// snapshots for the background, textures for rows and the properties dialog gallery, and pixbufs for the MPRIS thumbnailizer.
var Cover = GObject.registerClass({
    GTypeName: `${pkg.shortName}CoverPaintable`,
    Implements: [Gdk.Paintable],
    Properties: {
        'blur-radius': GObject.ParamSpec.double(
            'blur-radius', "Blur radius", "The blur radius to render the background cover with",
            GObject.ParamFlags.READWRITE, 0, 100, 0
        ),
        'bytes': GObject.ParamSpec.boxed(
            'bytes', "Bytes", "The bytes containing the cover data",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GLib.Bytes.$gtype
        ),
        'fade': GObject.ParamSpec.boolean(
            'fade', "Fade", "Whether the background cover must be fade",
            GObject.ParamFlags.READWRITE, false
        ),
        'file': GObject.ParamSpec.object(
            'file', "File", "The cover file",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, Gio.File.$gtype
        ),
        'is-dark': GObject.ParamSpec.boolean(
            'is-dark', "Is dark", "Whether the cover is dark",
            GObject.ParamFlags.READABLE, false
        ),
        'texture': GObject.ParamSpec.object(
            'texture', "Texture", "The cover texture",
            GObject.ParamFlags.READABLE, Gdk.Texture.$gtype
        ),
    },
}, class extends GObject.Object {
    get _fadePixbuf() {
        let pixbuf = this._pixbuf;
        let fadePixbuf = pixbuf.copy();
        pixbuf.copy_options(fadePixbuf);
        pixbuf.saturate_and_pixelate(fadePixbuf, 1, true);

        return fadePixbuf;
    }

    get _fadeTexture() {
        return this.__fadeTexture || (this.__fadeTexture = Gdk.Texture.new_for_pixbuf(this._fadePixbuf));
    }

    get _pixbuf() {
        if (this.__pixbuf)
            return this.__pixbuf;

        if (this.bytes) {
            let loader = new GdkPixbuf.PixbufLoader();
            loader.write_bytes(this.bytes);
            loader.close();
            return loader.get_pixbuf();
        } else if (this.file) {
            try {
                let stream = this.file.read(null);
                this.__pixbuf = GdkPixbuf.Pixbuf.new_from_stream(stream, null);
                stream.close(null);
                return this.__pixbuf;
            } catch(e) {
                pkg.debug ? logError(e) : log(e.message);
            }
        }
    }

    get _rgba() {
        if (!this.__rgba) {
            let pixbuf;
            if (this.bytes) {
                let loader = new GdkPixbuf.PixbufLoader();
                loader.set_size(1, 1);
                loader.write_bytes(this.bytes);
                loader.close();
                pixbuf = loader.get_pixbuf();
            } else if (this.file) {
                pixbuf = this._pixbuf.scale_simple(1, 1, GdkPixbuf.InterpType.BILINEAR);
            }

            let pixels = pixbuf.get_pixels();
            this.__rgba = new Gdk.RGBA();
            this.__rgba.parse(`rgb(${pixels[0]}, ${pixels[1]}, ${pixels[2]})`);
        }

        return this.__rgba;
    }

    vfunc_snapshot(snapshot, width, height) {
        let texture = this.fade ? this._fadeTexture : this.texture;
        let allocationBounds = new Graphene.Rect().init(0, 0, width, height);
        let centerBounds = allocationBounds, offsetX = 0, offsetY = 0;

        // Compute the bounds of the centered texture.
        if (texture.width / texture.height < width / height) {
            centerBounds = allocationBounds.inset_r((width - height * texture.width / texture.height) / 2, 0).round_extents();
            offsetX = centerBounds.get_width();
        } else if (texture.width / texture.height != width / height) {
            centerBounds = allocationBounds.inset_r(0, (height - width * texture.height / texture.width) / 2).round_extents();
            offsetY = centerBounds.get_height();
        }

        if (this.blurRadius)
            snapshot.push_blur(this.blurRadius);

        // First extend and paint the texture to the whole surface with reflection effect,
        // similarly to what 'cr.getSource().setExtend(Cairo.Extend.REFLECT)' was doing.
        snapshot.push_repeat(allocationBounds, null);
        repeat: {
            snapshot.append_texture(texture, centerBounds);

            if (centerBounds == allocationBounds)
                break repeat;

            snapshot.save();
            snapshot.rotate_3d(180, Graphene.Vec3[offsetX ? 'y_axis' : 'x_axis']());
            snapshot.translate(new Graphene.Point().init(offsetX && -width, offsetY && -height));
            snapshot.push_clip(centerBounds.offset_r(offsetX / 2, offsetY / 2).round_extents());
            snapshot.append_texture(texture, centerBounds.offset_r(offsetX, offsetY));
            snapshot.pop();
            snapshot.push_clip(centerBounds.offset_r(-offsetX / 2, -offsetY / 2).round_extents());
            snapshot.append_texture(texture, centerBounds.offset_r(-offsetX, -offsetY));
            snapshot.pop();
            snapshot.restore();
        }
        snapshot.pop();

        // Then add an opacity-high strong color on the reflected regions.
        if (centerBounds != allocationBounds) {
            let color = Object.assign(this._rgba.copy(), { alpha: 0.85 });
            snapshot.append_color(color, allocationBounds.offset_r(offsetX && (centerBounds.get_x() + offsetX), offsetY && (centerBounds.get_y() + offsetY)).intersection(allocationBounds)[1]);
            snapshot.append_color(color, allocationBounds.offset_r(offsetX && - (centerBounds.get_x() + offsetX), offsetY && - (centerBounds.get_y() + offsetY)).intersection(allocationBounds)[1]);
        }

        // Finally add an opacity-low strong color on the whole surface.
        let color = Object.assign(this._rgba.copy(), { alpha: 0.20 });
        snapshot.append_color(color, allocationBounds);

        if (this.blurRadius)
            snapshot.pop();
    }

    get blurRadius() {
        return this._blurRadius || 0;
    }

    set blurRadius(blurRadius) {
        if (this.blurRadius != blurRadius) {
            this._blurRadius = blurRadius;
            this.notify('blur-radius');
            this.invalidate_contents();
        }
    }

    get fade() {
        return this._fade || false;
    }

    set fade(fade) {
        if (this.fade != fade) {
            this._fade = fade;
            this.notify('fade');
            this.invalidate_contents();
        }
    }

    get isDark() {
        return (this._rgba.red + this._rgba.green + this._rgba.blue) / 3 < 0.5;
    }

    get texture() {
        return this._texture || (this._texture = this._pixbuf ? Gdk.Texture.new_for_pixbuf(this._pixbuf) : null);
    }

    // For MPRIS thumbnailizer.
    getPixbuf(size) {
        if (this.bytes) {
            let loader = new GdkPixbuf.PixbufLoader();
            loader.connect('size-prepared', (loader, coverWidth, coverHeight) => {
                let ratio = coverWidth / coverHeight;
                if (ratio > 1)
                    loader.set_size(size, size / ratio);
                else
                    loader.set_size(size * ratio, size);
            });
            loader.write_bytes(this.bytes);
            loader.close();
            return loader.get_pixbuf();
        } else if (this.file) {
            let pixbuf = this._pixbuf;
            let ratio = pixbuf.width / pixbuf.height;
            if (ratio > 1)
                return pixbuf.scale_simple(size, size / ratio, GdkPixbuf.InterpType.BILINEAR);
            else
                return pixbuf.scale_simple(size * ratio, size, GdkPixbuf.InterpType.BILINEAR);
        } else {
            throw new Error("No cover pixbuf");
        }
    }
});

/*
 * with 128 bands:
 * band 0: 62.5 Hz
 * band 1: 187.5 Hz (* 3)
 * band 4: 562.5 Hz (* 3**2)
 * band 13: 1687.5 Hz (* 3**3)
 * band 40: 5062.5 Hz (* 3**4)
 * band 95: 11937.5 Hz
 * band 106: 13312.5 Hz
 * band 115: 14437.5 Hz
 * band 121: 15187.5 Hz (* 3**5) --> very low
 *
 * AUDIO_FREQUENCE = 32000
 * frequence = ((AUDIO_FREQUENCE / 2) * band + AUDIO_FREQUENCE / 4) / SPECTRUM_BANDS
 * See Spectrum plugin web documentation.
*/

// This paintable doesn't aim to provide any real audio spectral information, but a visual and nice playing indicator.
// Given a pango context and an aspect ratio, the spectrum will be rendered according to the widget font size.
// Given a style context, the spectrum will be rendered with the widget color.
// The 'fallback' class is added to the style context if there are no magnitudes, 'spectrum' otherwise.
var Spectrum = GObject.registerClass({
    GTypeName: `${pkg.shortName}SpectrumPaintable`,
    Implements: [Gdk.Paintable],
    Properties: {
        'aspect-ratio': GObject.ParamSpec.double(
            'aspect-ratio', "Aspect ratio", "The aspect ratio to render the spectrum with",
            GObject.ParamFlags.READWRITE, 0.5, 4, 1.4
        ),
        'magnitudes': GObject.ParamSpec.boxed(
            'magnitudes', "Magnitudes", "The magnitudes supplied by the spectrum element of the player",
            GObject.ParamFlags.WRITABLE, GObject.ValueArray.$gtype
        ),
        'pango-context': GObject.ParamSpec.object(
            'pango-context', "Pango context", "The pango context of the widget that uses the paintable",
            GObject.ParamFlags.READWRITE, Pango.Context.$gtype
        ),
        'style-context': GObject.ParamSpec.object(
            'style-context', "Style context", "The style context of the widget that uses the paintable",
            GObject.ParamFlags.READWRITE, Gtk.StyleContext.$gtype
        ),
    },
}, class extends GObject.Object {

    static BANDS_NUMBER = 128;
    static THRESHOLD = -100;
    static _BANDS_KEEP = [0, 1, 4, 13, 40, 95];
    static _BAR_RATIO = 1.5; // horizontal ratio full/empty

    get _cancellable() {
        if (!this.__cancellable || this.__cancellable.cancelled)
            this.__cancellable = {};

        return this.__cancellable;
    }

    get _color() {
        return this.styleContext?.get_color() || new Gdk.RGBA({ alpha: 1 });
    }

    get _height() {
        return this.pangoContext?.get_font_description().get_size() / Pango.SCALE || 0;
    }

    vfunc_get_flags() {
        return Gdk.PaintableFlags.SIZE;
    }

    vfunc_get_intrinsic_height() {
        return this._height;
    }

    vfunc_get_intrinsic_width() {
        return this.aspectRatio * this._height;
    }

    vfunc_snapshot(snapshot, width, height) {
        if (!this._magnitudes)
            return;

        let values = Spectrum._BANDS_KEEP
            .map(band => band < this._magnitudes.n_values ? this._magnitudes.get_nth(band) : Spectrum.THRESHOLD)
            .map((value, index) => Math.pow(10, value / 20) * Math.pow(3, index))
            .map(value => Math.round(value * 500 + 20) / 100);

        let cr = snapshot.append_cairo(new Graphene.Rect().init(0, 0, width, height));

        Gdk.cairo_set_source_rgba(cr, this._color);

        let r = Spectrum._BAR_RATIO;
        let w = width / ((1 + r) * values.length - 1);
        for (let i = 0; i < values.length; i++) {
            let h = values[i] * height;
            cr.rectangle((1 + r) * w * i, height, w * r, -h);
        }

        cr.fill();
        cr.$dispose();
    }

    on_notify(pspec) {
        if (pspec.get_name() == 'style-context') {
            this.styleContext?.add_class(this._magnitudes ? 'spectrum' : 'fallback');
            this.styleContext?.remove_class(this._magnitudes ? 'fallback' : 'spectrum');
        }
    }

    set magnitudes(magnitudes) {
        this._magnitudes = magnitudes;

        this.styleContext?.add_class(magnitudes ? 'spectrum' : 'fallback');
        this.styleContext?.remove_class(magnitudes ? 'fallback' : 'spectrum');

        if (!magnitudes)
            this.cancel();

        this.invalidate_contents();
    }

    cancel() {
        if (this.__cancellable)
            this.__cancellable.cancelled = true;

        this._pendings = [];
        this._paused = false;
    }

    pause() {
        this._paused = true;
    }

    unpause(currentTime) {
        this._pendings?.forEach(([displayTime, magnitudes]) => {
            this.addDelayedMagnitudes(currentTime, displayTime, magnitudes);
        });

        this._pendings = [];
        this._paused = false;
    }

    // Times are in ms.
    addDelayedMagnitudes(currentTime, displayTime, magnitudes) {
        if (displayTime - currentTime < 0 || displayTime - currentTime > 5000)
            return;

        let timeout = Math.max(displayTime - currentTime - 50, 0);
        let cancellable = this._cancellable;

        GLib.timeout_add(GLib.PRIORITY_DEFAULT, timeout, () => {
            if (cancellable.cancelled)
                return GLib.SOURCE_REMOVE;

            if (this._paused)
                (this._pendings || (this._pendings = [])).push([displayTime, magnitudes]);
            else
                this.magnitudes = magnitudes;
        });
    }
});
