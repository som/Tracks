/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported main */

pkg.initGettext();
pkg.initFormat();
pkg.require({
    'Gdk': '4.0',
    'Gio': '2.0',
    'Gtk': '4.0',
});

const { Gio } = imports.gi;

// Adapted from famous 'convenience.js' (Giovanni Campagna);
// Build settings from build directory if the schema is not installed in '/usr/share/glib-2.0/schemas/'.
const getSettings = function(sub) {
    let schema = pkg.name + (sub ? `.${sub}` : '');
    try {
        return new Gio.Settings({ schema });
    } catch(e) {
        let schemaDirPath = `${pkg.datadir}/glib-2.0/schemas`;
        let schemaDir = Gio.File.new_for_path(schemaDirPath);

        const GioSSS = Gio.SettingsSchemaSource;

        let schemaSource = schemaDir.query_exists(null) ?
            GioSSS.new_from_directory(schemaDir.get_path(), GioSSS.get_default(), false) :
            GioSSS.get_default();

        let schemaObj = schemaSource.lookup(schema, true);
        if (!schemaObj)
            throw new Error(`Schema ${schema} could not be found in ${schemaDirPath}`);

        return new Gio.Settings({ settings_schema: schemaObj });
    }
};

pkg.settings = getSettings();
pkg.shortcutSettings = getSettings('shortcuts');
pkg.namePath = pkg._makeNamePath(pkg.name);

// available pkg properties:
// name, namePath, version, prefix, libdir, datadir,
// shortName, styleClass, debug
// settings, shortcutSettings

// Override _ so that "" is not translated.
const _Legacy = _;
_ = function(string) {
    return string && _Legacy(string);
};

// Do not import modules before defining pkg.namePath.
const { Application } = imports.application;

var main = function(argv) {
    return new Application().run(argv);
};
