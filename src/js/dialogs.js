/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported AboutDialog, FileChooserDialog, PlaylistLoaderDialog, PlaylistSaverDialog, PrefsDialog, PropsDialog, ShortcutsWindow */

const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const Gettext = imports.gettext;
const _GTK = imports.gettext.domain('gtk40').gettext;

const getValidArray = function(object, key) {
    return object[key] && object[key] instanceof Array && object[key].length && object[key];
};

let MINUTE, HOUR, DAY, WEEK = 7 * (DAY = 24 * (HOUR = 60 * (MINUTE = 60 * 1000)));

const formatDate = function(isoString) {
    let date = new Date(isoString);
    let diff = Date.now() - date.getTime();

    if (diff < HOUR)
        return new Intl.RelativeTimeFormat('default', { numeric: 'auto' }).format(-Math.floor(diff / MINUTE), 'minute');
    else if (diff < DAY)
        return new Intl.RelativeTimeFormat('default', { numeric: 'auto' }).format(-Math.floor(diff / HOUR), 'hour');
    else if (diff < WEEK)
        return new Intl.RelativeTimeFormat('default', { numeric: 'auto' }).format(-Math.floor(diff / DAY), 'day');

    return new Intl.DateTimeFormat('default', { year: 'numeric', month: 'short', day: 'numeric' }).format(date);
};

var AboutDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}AboutDialog`,
}, class extends Gtk.AboutDialog {
    _init(params) {
        let [success_, contents] = Gio.File.new_for_uri(`resource://${params.transientFor.application.resource_base_path}/about.json`).load_contents(null);
        let metadata = JSON.parse(contents.toLiteral());

        super._init(Object.assign({
            'logo-icon-name': metadata['logo-icon-name'] || null,
            'program-name': _(metadata['program-name']) || null,
            'version': metadata['version'] || null,
            'comments': _(metadata['comments']) || null,
            'website': metadata['website'] || null,
            'website-label': metadata['website-label'] || null,
            'copyright': metadata['copyright'] || null,
            'license-type': Gtk.License[metadata['license-type'] || 'UNKNOWN'] || null,
            'authors': getValidArray(metadata, 'authors') || null,
            'documenters': getValidArray(metadata, 'documenters') || null,
            'artists': getValidArray(metadata, 'artists') || null,
            'translator-credits': _("translator-credits"),
        }, params));
        this.add_css_class(pkg.styleClass);
    }
});

const getMatchStrings = function(string) {
    return GLib.str_tokenize_and_fold(string, null).map(token => token.toString()).filter(tokenString => tokenString != '');
};

const completionMatchFunction = function(completion, searchTerm, iter) {
    let storeTerm = completion.get_model().get_value(iter, 0);
    return getMatchStrings(searchTerm).some(searchString => {
        return getMatchStrings(storeTerm).some(storeString => storeString.includes(searchString));
    });
};

// A dialog that allows to open a file, a folder or an address.
var FileChooserDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}FileChooserDialog`,
}, class extends Gtk.FileChooserDialog {
    _init(params) {
        super._init(Object.assign({
            title: _("Select music files"),
            action: Gtk.FileChooserAction.OPEN,
            useHeaderBar: true, selectMultiple: true,
            name: 'Chooser',
        }, params));
        this.add_css_class(pkg.styleClass);

        let audioFilter = new Gtk.FileFilter();
        audioFilter.add_mime_type('audio/*');
        audioFilter.set_name(_("Audio files"));

        let videoFilter = new Gtk.FileFilter();
        videoFilter.add_mime_type('video/*');
        videoFilter.set_name(_("Video files"));

        let allFilter = new Gtk.FileFilter();
        allFilter.add_pattern('*');
        allFilter.set_name(_("All files"));

        this.add_filter(audioFilter);
        this.add_filter(videoFilter);
        this.add_filter(allFilter);

        this.add_choice('recursive', _("When adding a folder, browse it recursively to add files"), null, null);

        this._openResponse = 1;
        this._addressResponse = 2;
        this.add_button(_GTK("_Cancel"), Gtk.ResponseType.CANCEL);
        this.add_button(_GTK("_Open"), this._openResponse);

        let currentFolder = this.transientFor.currentFolder || Gio.File.new_for_path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC));
        // currentFolder may be null if music directory is not available.
        if (currentFolder)
            this.set_current_folder(currentFolder);

        this.addressEntry = new Gtk.Entry({
            placeholder_text: "https://…", tooltipText: _("Enter file or stream address"),
            primaryIconName: 'network-transmit-receive-symbolic', primaryIconActivatable: false,
            inputPurpose: Gtk.InputPurpose.URL, widthChars: 45,
            ariaLabel: _("Address entry"),
        });

        let model = new Gtk.ListStore();
        model.set_column_types([GObject.TYPE_STRING]);
        if (!model.insert_with_values)
            model.insert_with_values = model.insert_with_valuesv;
        Gtk.RecentManager.get_default().get_items()
            .filter(item => item.has_application(GLib.get_application_name()))
            .sort((a, b) => b.get_modified() - a.get_modified())
            .forEach(item => model.insert_with_values(-1, [0], [item.get_uri_display()]));

        let completion = new Gtk.EntryCompletion({ model, minimumKeyLength: 2 });
        this.addressEntry.set_completion(completion);
        completion.set_text_column(0);
        completion.set_match_func(completionMatchFunction);

        this.addressEntry.connect('changed', this._onAddressChanged.bind(this));
        this.addressEntry.connect('activate', this._onAddressActivated.bind(this));

        let focusController = new Gtk.EventControllerFocus({ widget: this.addressEntry });
        focusController.connect('enter', this._onAddressFocused.bind(this));
        focusController.connect('leave', this._updateOpenButton.bind(this));

        let keyController = new Gtk.EventControllerKey({ widget: this.addressEntry });
        keyController.connect('key-pressed', this._onAddressKeyPressed.bind(this));
    }

    vfunc_map() {
        super.vfunc_map();

        this.get_widget_for_response(this._openResponse).add_css_class('suggested-action');
        this.get_header_bar().set_title_widget(this.addressEntry);
    }

    _updateOpenButton() {
        this.get_widget_for_response(this._openResponse).set_sensitive(this.get_file() || this.addressText && this.addressText.length >= 3);
    }

    _onAddressKeyPressed(controller_, keyval) {
        // Do not close the dialog window when pressing the 'Escape' key in the address entry.
        if (keyval == Gdk.KEY_Escape) {
            this.get_widget_for_response(Gtk.ResponseType.CANCEL).grab_focus();
            return Gdk.EVENT_STOP;
        }
        return Gdk.EVENT_PROPAGATE;
    }

    _onAddressFocused() {
        this.set_file(this.get_current_folder().get_child('unselect_all_files'));
        this._updateOpenButton();
        return Gdk.EVENT_PROPAGATE;
    }

    _onAddressActivated(entry) {
        if (entry.text.trim().length >= 3)
            this.response(this._addressResponse);
    }

    _onAddressChanged(entry) {
        this.addressText = entry.text.trim();
        this._updateOpenButton();
    }

    _getAddressFile() {
        let address = this.addressText;
        if (GLib.uri_parse_scheme(address)) {
            return Gio.File.new_for_uri(address);
        } else {
            if (address.slice(0, 2) == '~/')
                address = GLib.build_filenamev([GLib.get_home_dir(), address.slice(2)]);

            if (address.slice(0, 1) == '/' || address.slice(0, 2) == './' || address.slice(0, 3) == '../')
                return Gio.File.new_for_commandline_arg_and_cwd(address, this.get_current_folder().get_path());
            else
                return Gio.File.new_for_uri(`https://${address}`);
        }
    }

    vfunc_response(response) {
        let files =
            response == this._openResponse ? (this.get_file() ? [...this.get_files()] : [this._getAddressFile()]) :
            response == this._addressResponse ? [this._getAddressFile()] :
            [];
        let window = this.transientFor;
        let recursive = this.get_choice('recursive') == 'true';

        window.currentFolder = this.get_current_folder();
        this.destroy();
        GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
            if (files.length)
                window.scanner.scanFiles(files, recursive);
            return GLib.SOURCE_REMOVE;
        });
    }
});

const ExportPlaylistDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}ExportPlaylistDialog`,
}, class extends Gtk.FileChooserDialog {
    _init(playlistDialog, editName, exportCallback) {
        this.exportCallback = exportCallback;
        super._init({
            title: _("Export the playlist"),
            action: Gtk.FileChooserAction.SAVE,
            transientFor: playlistDialog, modal: true,
            useHeaderBar: true,
        });

        this.set_current_folder(Gio.File.new_for_path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC) || ""));
        this.set_current_name(`${editName}.jspf`);

        let jspfFilter = new Gtk.FileFilter();
        jspfFilter.add_pattern('*.jspf');
        jspfFilter.set_name('*.jspf');

        let allFilter = new Gtk.FileFilter();
        allFilter.add_pattern('*');
        allFilter.set_name(_("All files"));

        this.add_filter(jspfFilter);
        this.add_filter(allFilter);

        this.add_button(_GTK("_Cancel"), Gtk.ResponseType.CANCEL);
        this.add_button(_GTK("_Save"), Gtk.ResponseType.ACCEPT);
    }

    vfunc_response(response) {
        let file = this.get_file();
        this.destroy();

        if (response == Gtk.ResponseType.ACCEPT)
            this.exportCallback(file);
    }
});

// Abstract class for PlaylistLoaderDialog and PlaylistSaverDialog.
const PlaylistDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistDialog`,
}, class extends Gtk.Dialog {
    _init(params, actionLabel, entry) {
        super._init(Object.assign({
            defaultHeight: 0.75 * params.transientFor.get_surface().get_height(),
            useHeaderBar: true,
            name: 'Playlists',
        }, params));
        this.add_css_class(pkg.styleClass);

        this.add_button(_GTK("_Cancel"), Gtk.ResponseType.CANCEL);
        this.add_button(actionLabel, 1);
        this.get_widget_for_response(1).add_css_class('suggested-action');
        this.get_widget_for_response(1).set_sensitive(false);

        this.entry = entry;
        this.entry.connect('changed', this._onEntryChanged.bind(this));
        this.entry.connect('activate', this._onEntryActivated.bind(this));

        let keyController = new Gtk.EventControllerKey({ widget: this.entry });
        keyController.connect('key-pressed', this._onEntryKeyPressed.bind(this));

        this.listBox = new Gtk.ListBox({ activateOnSingleClick: false, cssClasses: ['frame', 'rich-list', 'separators'] });
        this.listBox.connect('row-selected', this._onRowSelected.bind(this));
        this.listBox.connect('row-activated', this._onRowActivated.bind(this));

        [...this.transientFor.playlists].sort((playlist1, playlist2) => {
            return playlist2.date.localeCompare(playlist1.date);
        }).forEach(playlist => {
            let lengthText = _("%d %s").format(playlist.get_n_items(), Gettext.ngettext("track", "tracks", playlist.get_n_items()));
            let descriptionText = _("%s, %s").format(lengthText, formatDate(playlist.date));

            let label, description, exportButton, deleteButton, row = new Gtk.ListBoxRow({
                ariaRoledescription: _("Select the playlist"),
                child: new Gtk.Box().with(
                    new Gtk.Box({
                        orientation: Gtk.Orientation.VERTICAL, hexpand: true,
                    }).with(
                        label = new Gtk.Label({
                            label: playlist.displayName,
                            xalign: 0, ellipsize: 2,
                            cssClasses: ['title'],
                        }),
                        description = new Gtk.Label({
                            label: descriptionText,
                            xalign: 0, ellipsize: 2,
                            cssClasses: ['dim-label'],
                        })
                    ),
                    exportButton = new Gtk.Button({
                        iconName: 'document-save-symbolic',
                        tooltipText: _("Export the playlist"),
                        valign: Gtk.Align.CENTER,
                    }),
                    deleteButton = new Gtk.Button({
                        iconName: 'list-remove-symbolic',
                        tooltipText: _("Delete the playlist"),
                        valign: Gtk.Align.CENTER,
                        cssClasses: ['image-button', 'destructive-action'],
                    })
                ),
                ariaLabelledby: [label],
                ariaDescribedby: [description],
            });

            row.playlist = playlist;

            exportButton.connect('clicked', this._onExportButtonClicked.bind(this, playlist));
            deleteButton.connect('clicked', this._onDeleteButtonClicked.bind(this, row));

            this.listBox.append(row);
        });

        this.currentPlaylist = this.transientFor.selection.playlist;

        this.get_content_area().append(new Gtk.ScrolledWindow({
            hexpand: true, propagateNaturalWidth: true, propagateNaturalHeight: true,
            child: new Gtk.Box({
                orientation: Gtk.Orientation.VERTICAL, halign: Gtk.Align.CENTER,
                name: 'Content',
            }).with(
                this.entry,
                new Gtk.Frame({
                    labelWidget: new Gtk.Label({ label: _("Playlists"), cssClasses: ['dim-label'] }),
                    child: this.listBox,
                })
            ),
        }));
    }

    _onEntryKeyPressed(keyController_, keyval) {
        // Do not close the dialog window when pressing the 'Escape' key in the entry.
        if (keyval == Gdk.KEY_Escape) {
            this.get_widget_for_response(Gtk.ResponseType.CANCEL).grab_focus();
            return Gdk.EVENT_STOP;
        }
        return Gdk.EVENT_PROPAGATE;
    }

    _onExportButtonClicked(playlist) {
        let exportCallback = file => {
            playlist.exportAsync(file).catch(error => {
                this.transientFor.notifyError(error.message);
            }).catch(logError);
        };

        new ExportPlaylistDialog(this, playlist.editName, exportCallback).show();
    }

    _onDeleteButtonClicked(row) {
        row.playlist.deleteAsync().then(deleted => {
            if (deleted)
                this.listBox.remove(row);
        }).catch(error => {
            this.transientFor.notifyError(error.message);
        }).catch(logError);
    }
});

var PlaylistLoaderDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistLoaderDialog`,
}, class extends PlaylistDialog {
    _init(params) {
        let entry = new Gtk.SearchEntry();
        super._init(params, _GTK("_OK"), entry);

        this.title = _("Load a playlist");
        this.listBox.set_filter_func(this._onListBoxFiltered.bind(this));
        this.entry.ariaControls = [this.listBox];
    }

    _onListBoxFiltered(row) {
        return GLib.str_match_string(this.entry.text.trim(), row.playlist.editName, false);
    }

    _onEntryChanged(entry) {
        this.listBox.invalidate_filter();
    }

    _onEntryActivated(entry) {
        // Nothing.
    }

    _onRowActivated(listBox, row) {
        this.response(1);
    }

    _onRowSelected(listBox, row) {
        if (row) {
            this.transientFor.selection.playlist = row.playlist;
            this.get_widget_for_response(1).set_sensitive(true);
        }
    }

    vfunc_response(response) {
        let sensitive = this.get_widget_for_response(1).sensitive;
        this.destroy();

        if (response != 1 && sensitive)
            this.transientFor.selection.playlist = this.currentPlaylist;
        else if (response == 1 && sensitive)
            this.transientFor.selection.activatePlaylist();
    }
});

var PlaylistSaverDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistSaverDialog`,
}, class extends PlaylistDialog {
    _init(params) {
        let entry = new Gtk.Entry({
            primaryIconName: 'document-save-symbolic', primaryIconActivatable: false,
            tooltipText: _("Enter a name to save the playlist with"),
            ariaLabel: _("Save entry"),
        });
        super._init(params, _GTK("_Save"), entry);

        this.title = _("Save the current playlist");

        let originalPlaylist = this.transientFor.playlists.getPlaylist(this.currentPlaylist.name);
        if (originalPlaylist) {
            let displayName = originalPlaylist.equal(this.currentPlaylist) ?
                originalPlaylist.displayName : `* ${originalPlaylist.displayName}`;

            this.get_header_bar().titleWidget = new Gtk.Box({
                orientation: Gtk.Orientation.VERTICAL,
                valign: Gtk.Align.CENTER,
            }).with(
                new Gtk.Label({ label: this.title, cssClasses: ['title'] }),
                new Gtk.Label({ label: displayName, cssClasses: ['subtitle'] })
            );
        }
    }

    _onEntryChanged(entry) {
        this.get_widget_for_response(1).set_sensitive(Boolean(entry.text.trim().length));
        [...this.listBox].forEach(row => {
            if (row.playlist.editName == entry.text.trim())
                this.listBox.select_row(row);
            else
                this.listBox.unselect_row(row);
        });
    }

    _onEntryActivated(entry) {
        if (entry.text.trim().length)
            this.response(1);
    }

    _onRowActivated(listBox, row) {
        this.response(1);
    }

    _onRowSelected(listBox, row) {
        if (row && row.playlist.editName != this.entry.text.trim())
            this.entry.set_text(row.playlist.editName);
    }

    vfunc_response(response) {
        let name = this.entry.text.trim();
        this.destroy();

        if (response == 1) {
            this.currentPlaylist.saveAsync(name).then(saved => {
                if (saved)
                    this.transientFor.selection.playlist.name = name;
            }).catch(logError);
        }
    }
});

var PrefsDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}PrefsDialog`,
}, class extends Gtk.Dialog {
    _init(params) {
        super._init(Object.assign({
            title: _("Preferences"),
            name: 'Prefs',
        }, params));
        this.add_css_class(pkg.styleClass);

        let contentBox = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL, halign: Gtk.Align.CENTER,
            name: 'Content',
        });

        contentBox.append(new Gtk.Frame({
            labelWidget: new Gtk.Label({ label: _("Display") }),
            child: new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL }).with(
                ...['use-titlebar', 'display-covers', 'fade-covers', 'spectrum-indicator']
                    .map(key => this._getPrefWidget(key))
            ),
        }));

        contentBox.append(new Gtk.Frame({
            labelWidget: new Gtk.Label({ label: _("Playlist") }),
            child: new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL }).with(
                ...['play-continuously', 'remember-playlist']
                    .map(key => this._getPrefWidget(key))
            ),
        }));

        this.get_content_area().append(new Gtk.ScrolledWindow({
            hexpand: true, propagateNaturalWidth: true, propagateNaturalHeight: true,
            child: contentBox,
        }));
    }

    _getPrefWidget(key) {
        let grid = new Gtk.Grid();

        let check = new Gtk.CheckButton();
        pkg.settings.bind(key, check, 'active', Gio.SettingsBindFlags.DEFAULT);
        grid.attach(check, 0, 1, 1, 1);

        let label = new Gtk.Label({
            halign: Gtk.Align.START,
            label: _(pkg.settings.settings_schema.get_key(key).get_summary()),
        });
        grid.attach(label, 1, 1, 1, 1);
        check.ariaLabelledby = [label];

        if (pkg.settings.settings_schema.get_key(key).get_description()) {
            let description = new Gtk.Label({
                halign: Gtk.Align.START,
                label: pkg.settings.settings_schema.get_key(key).get_description(),
                cssClasses: ['dim-label'],
            });
            grid.attach(description, 1, 2, 1, 1);
            check.ariaDescribedby = [description];
        }

        if (key == 'spectrum-indicator')
            grid.sensitive = this.transientFor.player.hasSpectrumElement;

        return grid;
    }

    vfunc_response() {
        this.destroy();
    }
});

const CoverGallery = GObject.registerClass({
    GTypeName: `${pkg.shortName}CoverGallery`,
}, class extends Gtk.Overlay {
    _init(covers) {
        super._init({
            hexpand: true, vexpand: true,
            child: new Gtk.Stack({ ariaLabel: _("Covers") }),
        });

        covers.forEach((cover, index) => {
            this.child.add_child(new Gtk.Picture({
                paintable: cover.texture,
                ariaLabel: _("Cover"),
            }));
        });

        if (covers.length <= 1)
            return;

        let prevButton = new Gtk.Button({
            halign: Gtk.Align.START, valign: Gtk.Align.CENTER,
            iconName: 'go-previous',
            ariaControls: [this.child],
        });
        prevButton.connect('clicked', this._onPrevClicked.bind(this));
        this.add_overlay(prevButton);

        let nextButton = new Gtk.Button({
            halign: Gtk.Align.END, valign: Gtk.Align.CENTER,
            iconName: 'go-next',
            ariaControls: [this.child],
        });
        nextButton.connect('clicked', this._onNextClicked.bind(this));
        this.add_overlay(nextButton);

        this._updateButtonSensitivity(prevButton, nextButton);
    }

    _onPrevClicked(prevButton) {
        this.child.set_visible_child(this.child.get_visible_child().get_prev_sibling());
        this._updateButtonSensitivity(prevButton, prevButton.get_next_sibling());
    }

    _onNextClicked(nextButton) {
        this.child.set_visible_child(this.child.get_visible_child().get_next_sibling());
        this._updateButtonSensitivity(nextButton.get_prev_sibling(), nextButton);
    }

    _updateButtonSensitivity(prevButton, nextButton) {
        prevButton.sensitive = !!this.child.get_visible_child().get_prev_sibling();
        nextButton.sensitive = !!this.child.get_visible_child().get_next_sibling();
    }
});

var PropsDialog = GObject.registerClass({
    GTypeName: `${pkg.shortName}PropsDialog`,
    Properties: {
        'track': GObject.ParamSpec.object(
            'track', "Track", "The track whose properties are displayed",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Track`)
        ),
    },
}, class extends Gtk.Dialog {
    _init(params) {
        super._init(Object.assign({
            defaultWidth: 0.80 * params.transientFor.get_surface().get_width(),
            defaultHeight: 0.75 * params.transientFor.get_surface().get_height(),
            title: _("Properties"),
            name: 'Props',
        }, params));
        this.add_css_class(pkg.styleClass);

        this.notebook = new Gtk.Notebook({ hexpand: true, vexpand: true });
        this.get_content_area().append(this.notebook);

        let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
        let scrolledWindow = new Gtk.ScrolledWindow({ propagateNaturalWidth: true, propagateNaturalHeight: true, child: contentBox });
        this.notebook.append_page(scrolledWindow, new Gtk.Label({ label: _("Informations") }));

        let filename = Gio.File.new_for_uri(this.track.uri).get_parse_name();
        contentBox.append(this._filenameLabel = new Gtk.Label({ label: filename, xalign: 0, ellipsize: 2, selectable: true }));

        // website playlist uri
        if (this.track.info)
            contentBox.append(new Gtk.Label({ label: this.track.info, xalign: 0, ellipsize: 2, selectable: true }));

        // website playlist name
        if (this.track.annotation)
            contentBox.append(new Gtk.Label({ label: this.track.annotation, xalign: 0, ellipsize: 2, selectable: true }));

        contentBox.append(new Gtk.Frame({ labelWidget: new Gtk.Label({ label: _("General") }), child: this._generalGrid = new Gtk.Grid() }));
        contentBox.append(new Gtk.Frame({ labelWidget: new Gtk.Label({ label: _("Audio") }), child: this._audioGrid = new Gtk.Grid() }));

        // feed uri
        if (this.track.isWebsiteXml && this.track.info) {
            let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
            this.notebook.append_page(new Gtk.ScrolledWindow({ child: contentBox }), new Gtk.Label({ label: _("Podcast") }));
            contentBox.append(new Gtk.Label({ label: this.track.info, halign: Gtk.Align.START, ellipsize: 2, selectable: true }));

            // feed description
            if (this.track.annotation)
                contentBox.append(new Gtk.Label({ label: this.track.annotation, halign: Gtk.Align.START, useMarkup: true, wrap: true, selectable: true }));
        }

        this.track.getPropsAsync().then(this._onPropsReceived.bind(this)).catch(logError);

        if (this.track.isWebsiteHtml)
            this.track.getWebsitePropsAsync().then(this._onWebsitePropsReceived.bind(this)).catch(logError);
    }

    _onPropsReceived([generalProps, audioProps, covers, comment, lyrics]) {
        Object.keys(generalProps).forEach((key, index) => this._addProp(this._generalGrid, index, key, generalProps[key]));
        Object.keys(audioProps).forEach((key, index) => this._addProp(this._audioGrid, index, key, audioProps[key]));

        if (covers && covers.length) {
            let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
            this.notebook.append_page(new Gtk.ScrolledWindow({ child: contentBox }), new Gtk.Label({ label: _("Covers") }));
            contentBox.append(new Gtk.Box().with(new CoverGallery(covers)));
        }

        if (comment) {
            let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
            this.notebook.append_page(new Gtk.ScrolledWindow({ child: contentBox }), new Gtk.Label({ label: _("Comment") }));
            contentBox.append(new Gtk.Label({ label: comment, halign: Gtk.Align.START, wrap: true, selectable: true }));
        }

        if (lyrics) {
            let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
            this.notebook.append_page(new Gtk.ScrolledWindow({ child: contentBox }), new Gtk.Label({ label: _("Lyrics") }));
            contentBox.append(new Gtk.Label({ label: lyrics, halign: Gtk.Align.START, wrap: true, selectable: true }));
        }
    }

    _onWebsitePropsReceived([mediaUri, thumbnailUri, generalProps]) {
        this._filenameLabel.label = Gio.File.new_for_uri(mediaUri).get_parse_name();

        if (thumbnailUri) {
            try {
                let texture = new Gdk.Texture.new_from_file(Gio.File.new_for_uri(thumbnailUri));
                let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
                this.notebook.append_page(new Gtk.ScrolledWindow({ child: contentBox }), new Gtk.Label({ label: _("Covers") }));
                contentBox.append(new Gtk.Box().with(new CoverGallery([{ texture }])));
            } catch(e) {
                pkg.debug ? logError(e) : log(e.message);
            }
        }

        if (generalProps.WebsiteDescription) {
            let contentBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, name: 'Content' });
            this.notebook.append_page(new Gtk.ScrolledWindow({ child: contentBox }), new Gtk.Label({ label: _("Description") }));
            contentBox.append(new Gtk.Label({ label: generalProps.WebsiteDescription, halign: Gtk.Align.START, useMarkup: true, wrap: true, selectable: true }));
        }

        Object.keys(generalProps).filter(key => key != 'WebsiteDescription')
            .forEach((key, index) => this._addProp(this._generalGrid, index, key, generalProps[key]));
    }

    _addProp(grid, position, key, value) {
        let keyLabel = new Gtk.Label({ useMarkup: true, label: `<i>${_("%s:").format(key)}</i>`, xalign: 0 });
        grid.attach(keyLabel, 0, position, 1, 1);
        let valueLabel = new Gtk.Label({ useMarkup: true, label: value.toString(), xalign: 0, ellipsize: 2, selectable: true });
        grid.attach(valueLabel, 1, position, 1, 1);

        if (!this._sizeGroup)
            this._sizeGroup = new Gtk.SizeGroup();
        this._sizeGroup.add_widget(keyLabel);
    }

    vfunc_response() {
        this.destroy();
    }
});

const ShortcutsGroup = GObject.registerClass({
    GTypeName: `${pkg.shortName}ShortcutsGroup`,
}, class extends Gtk.ShortcutsGroup {
    _init(params) {
        this._accelSizeGroup = new Gtk.SizeGroup();
        this._titleSizeGroup = new Gtk.SizeGroup();

        super._init(Object.assign(params, {
            accelSizeGroup: this._accelSizeGroup,
            titleSizeGroup: this._titleSizeGroup,
        }));
    }

    addShortcut(shortcut) {
        shortcut.accelSizeGroup = this._accelSizeGroup;
        shortcut.titleSizeGroup = this._titleSizeGroup;

        this.append(shortcut);
    }
});

const ShortcutsSection = GObject.registerClass({
    GTypeName: `${pkg.shortName}ShortcutsSection`,
}, class extends Gtk.ShortcutsSection {
    _init(params) {
        super._init(params);

        let stack = this.get_first_child();
        let orientation = this.orientation == Gtk.Orientation.HORIZONTAL ? Gtk.Orientation.VERTICAL : Gtk.Orientation.HORIZONTAL;
        this._box = new Gtk.Box({ spacing: 22, orientation });
        stack.add_child(this._box);
        stack.set_visible_child(this._box);

        this._boxes = [];
    }

    // @index specifies the column for vertical orientation and the row for horizontal orientation.
    addGroup(group, index = 0) {
        if (!this._boxes[index]) {
            this._boxes[index] =  new Gtk.Box({ spacing: 22, orientation: this.orientation });
            this._box.append(this._boxes[index]);
        }

        this._boxes[index].append(group);
    }
});

var ShortcutsWindow = GObject.registerClass({
    GTypeName: `${pkg.shortName}ShortcutsWindow`,
}, class extends Gtk.ShortcutsWindow {
    _init(params) {
        super._init(Object.assign({
            resizable: true,
        }, params));
        this.add_css_class(pkg.styleClass);

        let section = new ShortcutsSection({ sectionName: 'shortcuts' });
        this.addSection(section);

        let generalGroup = new ShortcutsGroup({ title: _("General") });
        let playlistGroup = new ShortcutsGroup({ title: _("Playlist") });
        let playerGroup = new ShortcutsGroup({ title: _("Player") });

        section.addGroup(generalGroup);
        section.addGroup(playlistGroup);
        section.addGroup(playerGroup, 1);

        this._addShortcuts(generalGroup, 'quit', 'about', 'prefs', 'shortcuts');
        this._addShortcuts(playlistGroup, 'open-file-chooser', 'open-playlist-loader', 'open-playlist-saver', 'enable-search', 'view-display-row-controls', 'view-show-selected-row');
        this._addShortcuts(playerGroup, 'props', 'pause', 'skip-backward', 'skip-forward', 'go-backward', 'go-forward');
    }

    addSection(section) {
        let stack = this.child.get_last_child();
        stack.add_named(section, section.sectionName);
        stack.set_visible_child(section);
    }

    _addShortcuts(group, ...names) {
        let schema = pkg.shortcutSettings.settings_schema;

        names.forEach(name => {
            if (!schema.has_key(name))
                return;

            group.addShortcut(new Gtk.ShortcutsShortcut({
                title: _(schema.get_key(name).get_summary()),
                subtitle: _(schema.get_key(name).get_description()),
                accelerator: pkg.shortcutSettings.get_strv(name).join(' '),
            }));
        });
    }
});
