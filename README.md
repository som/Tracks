# Tracks

A simple audio player.

[![](data/screenshots/light-small.png)](data/screenshots/light.png)[![](data/screenshots/dark-small.png)](data/screenshots/dark.png)[![](data/screenshots/wide-small.png)](data/screenshots/wide.png)

## Features

* Local files and streams (websites, radios, podcast feeds, etc)
* Recursive folder discovering
* Internal playlists
* Text search to filter the playlist by title, artist, album and genre
* MPRIS
* Client-side decoration (CSD) or title bar

## Dependencies

Tracks has minimal dependencies. To resume, if you have **GJS**, **GStreamer** and **GTK4** you are good.

Dependencies:

* GdkPixbuf (gdk-pixbuf-2.0)
* GIO (gio-2.0)
* GJS (gjs-1.0, >= 1.67.3)
* GLib (glib-2.0)
* GObject (gobject-2.0)
* GStreamer (gstreamer-1.0)
* GStreamer Base Plugins Libraries (gstreamer-plugins-base-1.0)
* GStreamer Base Utils Library (gstreamer-pbutils-1.0)
* GTK4 (gtk4)

GdkPixbuf, GIO, GLib and GObject should come with GTK4.  
GStreamer is a plugin-based framework. Media playing capacities depend on the well-known base, good, ugly and bad plugins.

Optional:

* ~~GnomeDesktop (gnome-desktop-3.0)~~
* GStreamer Tag Library (gstreamer-tag-1.0)
* GStreamer Good Plugins
* youtube-dl

~~GnomeDesktop is a small library used to generate thumbnails for mpris clients.~~  
GStreamer Tag Library is not important and should come with GStreamer Base Utils Library.  
GStreamer Good Plugins is necessary to display the “spectral” playing indicator.  
youtube-dl extracts media uris from feeds and video-sharing websites.

## Install

<details>
    <summary>Host</summary>

Build and install:

```sh
meson build && cd build && ninja && sudo ninja install
```
  
Uninstall:

```sh
cd build && sudo ninja uninstall
sudo glib-compile-schemas /usr/local/share/glib-2.0/schemas
sudo update-desktop-database /usr/local/share/applications/
```

Clean the home after uninstalling:

```sh
dconf reset -f /org/codeberg/som/Tracks/
rm -rf ~/.local/share/tracks
```
</details>

<details>
    <summary>Flatpak</summary>

A `org.gnome.Sdk` runtime is required. The runtime versions can be edited in the JSON manifest to match preexisting ones and avoid unnecessary downloads.

Build and test:

```sh
flatpak-builder --force-clean build-dir org.codeberg.som.Tracks.json
flatpak-builder --run build-dir   org.codeberg.som.Tracks.json org.codeberg.som.Tracks
```

Build and install:

```sh
flatpak-builder --install --force-clean build-dir org.codeberg.som.Tracks.json
flatpak run org.codeberg.som.Tracks
```

Uninstall:

```sh
flatpak remove --delete-data org.codeberg.som.Tracks
```
</details>
